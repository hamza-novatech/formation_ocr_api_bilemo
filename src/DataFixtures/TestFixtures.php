<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @codeCoverageIgnore
 */
class TestFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $product = new Product;
        $product->setBrand('brandTest')
            ->setModel('modelTest')
            ->setReleaseDate(new DateTime())
            ->setPrice(100)
            ->setColor('colorTest')
            ->setStockQuantity('100')
            ->setDescription('descriptionTest');

        $customer = new Customer;
        $customer->setName('compagnyTest');

        $user = new User;
        $plainPassword = 'passwordTest';
        $encoded = $this->encoder->encodePassword($user, $plainPassword);
        $user->setFirstName('firstNameTest')
             ->setLastName('lastNameTest')
             ->setEmail('email@test.com')
             ->setPassword($encoded)
             ->setPhone('+330600000000')
             ->setAdress('adressTest')
             ->setCustomer($customer);

        $manager->persist($user);
        $manager->persist($customer);
        $manager->persist($product);

        $manager->flush();
    }
}
