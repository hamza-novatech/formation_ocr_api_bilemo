<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 25; $i++) {
            $product = new Product;

            $product->setBrand($faker->randomElement(['Apple', 'Samsung', 'Google', 'OnePlus', 'Huawei']))
                ->setModel($faker->word() . ' ' . $faker->numberBetween(6, 12))
                ->setReleaseDate($faker->dateTimeBetween('-2 years', 'now'))
                ->setPrice($faker->randomFloat(2, 250, 1300))
                ->setColor($faker->colorName())
                ->setStockQuantity($faker->numberBetween(1, 100))
                ->setDescription($faker->text(350));

            $manager->persist($product);
        }

        for ($e=0; $e < 10; $e++) {
            $customer = new Customer;

            $customer->setName($faker->company());

            for ($f=0; $f < 10; $f++) {
                $user = new User;

                $plainPassword = $faker->password();
                $encoded = $this->encoder->encodePassword($user, $plainPassword);
    
                $user->setFirstName($faker->firstName())
                     ->setLastName($faker->lastName())
                     ->setEmail($faker->email())
                     ->setPassword($encoded)
                     ->setPhone($faker->phoneNumber())
                     ->setAdress($faker->address())
                     ->setCustomer($customer);

                $manager->persist($user);
            }
        
            $manager->persist($customer);
        }

        $manager->flush();
    }
}
