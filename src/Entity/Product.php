<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ApiResource(
 *  collectionOperations={"GET"},
 *  itemOperations={"GET"},
 *  normalizationContext={
 *      "groups"={"products_read"}
 *  }
 * )
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_read"})
     * @Assert\NotBlank(message="La marque est obligatoire")
     * @ApiProperty(push=true)
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_read"})
     * @Assert\NotBlank(message="La modèle est obligatoire")
     * @ApiProperty(push=true)
     */
    private $model;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"products_read"})
     * @Assert\NotBlank(message="La date de sortie est obligatoire")
     * @ApiProperty(push=true)
     */
    private $releaseDate;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     * @Groups({"products_read"})
     * @Assert\NotBlank(message="Le prix est obligatoire")
     * @ApiProperty(push=true)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"products_read"})
     * @Assert\NotBlank(message="La couleur est obligatoire")
     * @ApiProperty(push=true)
     */
    private $color;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"products_read"})
     * @ApiProperty(push=true)
     */
    private $stockQuantity;

    /**
     * @ORM\Column(type="text")
     * @Groups({"products_read"})
     * @Assert\NotBlank(message="La description est obligatoire")
     * @ApiProperty(push=true)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getStockQuantity(): ?int
    {
        return $this->stockQuantity;
    }

    public function setStockQuantity(int $stockQuantity): self
    {
        $this->stockQuantity = $stockQuantity;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
