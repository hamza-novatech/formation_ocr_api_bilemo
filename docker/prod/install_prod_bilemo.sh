#!/bin/bash

# This script is used to install the Docker production environment for the BileMo REST API.

docker-compose up -d
docker exec www_bilemo_prod composer install
docker exec www_bilemo_prod php bin/console doctrine:database:drop --force
docker exec www_bilemo_prod bin/console doctrine:database:create
docker exec www_bilemo_prod php bin/console doctrine:migration:migrate --no-interaction
docker exec www_bilemo_prod php bin/console doctrine:fixtures:load -q