#!/bin/bash

# This script is used to launch the Docker development environment for the BileMo REST API.
# At each execution it deletes the previous database if it exists, then it imports a data set.


docker-compose --env-file ./.env.local up -d
docker exec www_bilemo_dev composer install
docker exec www_bilemo_dev php bin/console doctrine:database:drop --force
docker exec www_bilemo_dev bin/console doctrine:database:create
docker exec www_bilemo_dev php bin/console doctrine:migration:migrate --no-interaction
docker exec www_bilemo_dev php bin/console doctrine:fixtures:load -q