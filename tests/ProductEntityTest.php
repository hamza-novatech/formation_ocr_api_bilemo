<?php

namespace App\Tests;

use App\Entity\Product;
use DateTime;
use PHPUnit\Framework\TestCase;

class ProductEntityTest extends TestCase
{
    public function testProductWhenTheDataIsRight()
    {
        $product = new Product;

        $datetime = new DateTime();

        $product->setBrand('brand')
                ->setModel('model')
                ->setReleaseDate($datetime)
                ->setPrice(1000.00)
                ->setColor('color')
                ->setStockQuantity(100)
                ->setDescription('description');

        $this->assertTrue($product->getBrand() === 'brand');
        $this->assertTrue($product->getModel() === 'model');
        $this->assertTrue($product->getReleaseDate() === $datetime);
        $this->assertTrue($product->getPrice() == 1000.00);
        $this->assertTrue($product->getColor() === 'color');
        $this->assertTrue($product->getStockQuantity() == 100);
        $this->assertTrue($product->getDescription() === 'description');
    }

    public function testProductWhenTheDataIsWrong()
    {
        $product = new Product;

        $datetime = new DateTime();

        $product->setBrand('false')
                ->setModel('false')
                ->setReleaseDate(new DateTime())
                ->setPrice(10.00)
                ->setColor('false')
                ->setStockQuantity(1)
                ->setDescription('false');

        $this->assertFalse($product->getBrand() === 'brand');
        $this->assertFalse($product->getModel() === 'model');
        $this->assertFalse($product->getReleaseDate() === $datetime);
        $this->assertFalse($product->getPrice() == 1000.00);
        $this->assertFalse($product->getColor() === 'color');
        $this->assertFalse($product->getStockQuantity() == 100);
        $this->assertFalse($product->getDescription() === 'description');
    }

    public function testProductWhenTheDataIsEmpty()
    {
        $product = new Product;

        $this->assertEmpty($product->getBrand());
        $this->assertEmpty($product->getModel());
        $this->assertEmpty($product->getReleaseDate());
        $this->assertEmpty($product->getPrice());
        $this->assertEmpty($product->getColor());
        $this->assertEmpty($product->getStockQuantity());
        $this->assertEmpty($product->getDescription());
        $this->assertEmpty($product->getId());
    }
}
