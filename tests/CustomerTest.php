<?php

namespace App\Tests;

use App\Entity\Customer;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    public function testCustomerWhenTheDataIsRight()
    {
        $customer = new Customer;

        $user = new User;

        $customer->setName('name')
                 ->addUser($user);

        $this->assertTrue($customer->getName() === 'name');
        $this->assertContains($user, $customer->getUsers());
    }

    public function testCustomerWhenTheDataIsFalse()
    {
        $customer = new Customer;

        $user = new User;

        $customer->setName('false')
                 ->addUser(new User);

        $this->assertFalse($customer->getName() === 'name');
        $this->assertNotContains($user, $customer->getUsers());
    }

    public function testCustomerWhenTheDataIsEmpty()
    {
        $customer = new Customer;

        $this->assertEmpty($customer->getName());
        $this->assertEmpty($customer->getUsers());
        $this->assertEmpty($customer->getId());
    }

    public function testCustomerAddAndDeleteUser()
    {
        $user = new User;

        $customer = new Customer;
        $customer->addUser($user);

        $this->assertContains($user, $customer->getUsers());

        $customer->removeUser($user);

        $this->assertNotContains($user, $customer->getUsers());
    }
}
