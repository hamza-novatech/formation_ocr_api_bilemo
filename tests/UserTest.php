<?php

namespace App\Tests;

use App\Entity\Customer;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testUserWhenTheDataIsRight()
    {
        $user = new User;

        $customer = new Customer;

        $user->setFirstName('firstName')
             ->setLastName('lastName')
             ->setEmail('mail@mail.com')
             ->setPassword('password')
             ->setPhone('+0033060000000')
             ->setAdress('adress')
             ->setCustomer($customer);

        $this->assertTrue($user->getFirstName() === 'firstName');
        $this->assertTrue($user->getLastName() === 'lastName');
        $this->assertTrue($user->getEmail() === 'mail@mail.com');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getPhone() === '+0033060000000');
        $this->assertTrue($user->getAdress() === 'adress');
        $this->assertTrue($user->getCustomer() === $customer);
        $this->assertTrue($user->getUsername() === 'mail@mail.com');
        $this->assertTrue(['ROLE_USER'] === $user->getRoles());
    }

    public function testUserWhenTheDataIsFalse()
    {
        $user = new User;

        $user->setFirstName('false')
             ->setLastName('false')
             ->setEmail('false')
             ->setPassword('false')
             ->setPhone('false')
             ->setAdress('false')
             ->setCustomer(new Customer);

        $this->assertFalse($user->getFirstName() === 'firstName');
        $this->assertFalse($user->getLastName() === 'lastName');
        $this->assertFalse($user->getEmail() === 'mail@mail.com');
        $this->assertFalse($user->getPassword() === 'password');
        $this->assertFalse($user->getPhone() === '+0033060000000');
        $this->assertFalse($user->getAdress() === 'adress');
        $this->assertFalse($user->getCustomer() === new Customer);
    }

    public function testUserWhenTheDataIsEmpty()
    {
        $user = new User;

        $user->eraseCredentials();

        $this->assertEmpty($user->getFirstName());
        $this->assertEmpty($user->getLastName());
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getPhone());
        $this->assertEmpty($user->getAdress());
        $this->assertEmpty($user->getCustomer());
        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->getSalt());
    }

    public function testUserAddRole()
    {
        $user = new User;

        $user->setRoles(['TEST']);

        $this->assertTrue(['TEST', 'ROLE_USER'] === $user->getRoles());
    }
}
