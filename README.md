# BileMo REST API

Creation of an REST API with Symfony. As part of an OpenClassrooms training

[![pipeline status](https://gitlab.com/yoan.bernabeu/formation_ocr_api_bilemo/badges/master/pipeline.svg)](https://gitlab.com/yoan.bernabeu/formation_ocr_api_bilemo/-/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/5ea1d26807284a899bb40222b9e2e6d6)](https://www.codacy.com/manual/yoan.bernabeu/formation_ocr_api_bilemo/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=yoan.bernabeu/formation_ocr_api_bilemo&utm_campaign=Badge_Grade)

![image](readme.png)

* * *

## **Prerequisites**

-   Linux Debian / Linux Ubuntu
-   Docker
-   Docker-compose
-   OpenSSL

* * *

## **1. Installation of the development environment**

We provide a complete Docker development environment with the following:

-   Apache / Php container
-   Mysql container
-   Varnish Container (Cache)
-   Blackfire container (profiling)

### **1.1. Generate the SSH keys**

Set JWT_PASSPHRASE in .env and use it in the following commands :

```bash
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
chmod 755 -R config/jwt/
```

### **1.2. Start the development environment (with Fixtures) - Port 3000**

```bash
cd docker/dev
sh run_dev_bilemo.sh
```

### **1.3. Run Unit tests**

```bash
docker exec www_bilemo_dev php bin/phpunit --testdox
```

### **1.4. Run functional tests (With Postman)**

-   Install [Postman](https://www.postman.com/downloads/)
-   Open Postman
-   Click on File>Import
-   Choose file ./documentation/postman/API BileMo.postman_collection.json
-   Click on "Runner"
-   Choose "API BileMo"
-   Set the number of iterations
-   Click on "Run API BileMo" and enjoy the show !

* * *

## **2. Installation of the Production environment**

We provide a complete Docker Production environment with the following:

-   Apache / Php container
-   Mysql container
-   Varnish Container (Cache)

### **2.1. Generate the SSH keys**

Set JWT_PASSPHRASE in .env and use it in the following commands :

```bash
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
chmod 755 -R config/jwt/
```

### **2.2. Install the production environment (with Fixtures) -  Port 80**

To be run only for startup with fixtures.

```bash
cd docker/prod
sh install_prod_bilemo.sh
```

Remember to change the value of APP_ENV to PROD in environment variables or in .env file

### **2.3. Run Unit tests**

```bash
docker exec www_bilemo_prod php bin/phpunit --testdox
```

### **2.4. Run functional tests (With Postman)**

-   Install [Postman](https://www.postman.com/downloads/)
-   Open Postman
-   Click on File>Import
-   Choose file ./documentation/postman/API BileMo.postman_collection.json
-   Change with your production URL
-   Click on "Runner"
-   Choose "API BileMo"
-   Set the number of iterations
-   Click on "Run API BileMo" and enjoy the show !

* * *

## **License**

[MIT](LICENSE)
